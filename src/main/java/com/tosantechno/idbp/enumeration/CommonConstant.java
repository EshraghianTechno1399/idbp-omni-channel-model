package com.tosantechno.idbp.biz.Enum;

public class CommonConstant
{
    public enum ChannelType
    {
        MOBILE(0),
        VTM(1);

        private short value;
        ChannelType(int value)
        {
            this.value = (short) value;
        }
        public short getValue(){
            return (short) value;
        }

        public int getIndex()
        {
            return this.value;
        }

        public String getName (int index)
        {
            for ( ChannelType type: ChannelType.values() )
            {
                if (type.getIndex() == index )
                    return type.name();

            }
            return null;
        }
    }

}
