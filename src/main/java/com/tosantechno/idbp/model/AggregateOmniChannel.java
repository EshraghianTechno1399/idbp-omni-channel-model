package com.tosantechno.idbp.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

@Getter
@Setter
public class AggregateOmniChannel
{
    @Id
    private String code;
//    private Integer  serviceType;
    private String    max_step;
}
