package com.tosantechno.idbp.model;

import com.mongodb.lang.Nullable;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

import java.util.Date;


@Getter
@Setter
@Document("TotalOmniChannel")
@Component
public class OmniChannel
{
    @Id
    private  String  id;

    @Nullable
    private  Integer startChannel;
    @Nullable
    private  Integer serviceType;
    @Nullable
    private  Integer bankId;

    private  String    code;

    @Nullable
    private  String    externalCode;
    @Nullable
    private  Boolean status;
    @Nullable
    private  String  nationalCode;
    @Nullable
    private Date createDate;

    private  Integer step;

    private  String data;
}
