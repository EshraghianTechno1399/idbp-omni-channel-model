package com.tosantechno.idbp.model;


import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Getter
@Setter
@Component
public class OmniChannelStep
{
//    @MongoId
//    private String     id;
    private String     code;
    private Integer    step;
//    private Timestamp  timestamp;
    private String     data;
}
